This is the LT telephone project.

Contributors:
Prologue and Ch1: Mag
Ch2: rainlash
Ch3: Rasta
Ch4: SirSpencer
Ch5: Jumpants
Ch6: RandomWizard
ch7: Lord_Tweed
ch8: KD
ch9: SigmaRaven
ch10: OmegaX128
Ch11: Cepheus

Credits:
From the FE Graphics Repo:

Portraits:
Cloak Woman
Kyuzeth_Ilus
Renny
TheWildCard OC1
Vampyryte OC 1
XVI_Alexandra
ZoramineFae_5
PkMnBro1 - Hans
Smokeyguy77_1
Lenh
1(XPGamesNL)
Ambrosiac - Master Cultist
rainlash - Yarrow, Thrant, Ophie
XVI - Armorer
Zarg - Nialla
Laurent_lacroix - Sionn, Pitta, Buyer, Isador, Goibniu
Flasuban - Cistia, Shoebill
Can Dy - Accipiter
Kanna - Ayla, Caer, Striga
Generic Pretsel - Osbert, Knight
Rohan_Yubello - Prince Zackary
ZessDynamite - Abeni
Sweet Basil - Lady Con
lmr - Dullahan
L95 - Mogall
caringcarrot - Young Shoebill
Melia - Lim, Amada
RandomWizard - Bronach, Sadhbh, Moore, Cormorant, Kevin
Sdaht, LaurentLacroix - Lychee
RandomWizard, MeatOfJustice - Farl
Lenh - Steel Fox
JeyTheCount - Cook, Truk
Knabepicer - Ysbryd
WAve - Tiberius
Glaceo, Mr_Karkino - Female O'Neill
HyperGammaSpaces - Villager
KD - Brandon, Fess, Remri
Cravat - Bluejay
Its_Just_Jay - Faviyn
DerTheVaporeon - Luan
Klokinator - Jarze, Seliche
Sphealnuke - Byrne

Animations:
Serif and FE7if devs (Hector Milia)
RedBean (Celica)
Nuramon (Heavy Infantry, Gold Knight animation)
SALVAGED (Cavalier M and F, Knight, Paladin M)
Flasuban (Improved Falcoknight, Pegasi T1 repal v2 + Weapons)
SixThousandHulls (Pegasi T1 M)
Leo_link(Fighter, Ranger)
skitty (Merceneary Repalette)
More Eprhaim animations by DerTheVaporeon, Pikmin1211.
Cath Repal by Pikmin
Fancy Bard by MeatOfJustice
Ephraim Great Lord animations by eCut, Belle, and St.Jack
Rogue anims by Orihara_saki, Ukulele, SD9k
Fae to Myrrh Dragon by Teraspark
Axe Wyvern Rider - Mikey Seregon, Alfred Kamon
Female Brigand (Barbarian) - eCut, Skitty
Female Berserker (Armored) v2 Repal - eCut, Skitty, Serif
Leo-Style +Weapons Ranger - Leo_Link, SALVAGED, knabepicer, Jey the Count
Deacon v2 Repal +Weapons - Pikmin1211, GabrielKnight, Maiser6, Lisandra_Brave, TBA
Female Pupil - Pikmin1211
Mercenary Lord & NYZGamer - Dragoon
Nuramon - Void Dragon

Maps:
ZoramineFae
N246
LordGlenn
Swamp tileset - Hypergammaspaces
Frontier tileset - Flasuban, WAve, ZoramineFae
Lava Fields tileset - Hypergammaspaces, cecilyn
Snow Castle - Peerless
Snowy Bern - FEAW, ZoramineFae, Vennobennu
Castle Pandemonium - WAve

Icons:
EldritchA, Indogutsu Tenbuki, Tactics Ogre
Mag, NerysRhys, FFTA2
Ershkigal - Dagger Sprites
XVI
LisandraBrave
GabrielKnight
Rasta - Knife wexp icon
Celice - Ballista weapon icons

Map Sprites:
TBA Halberdier
Red Bean Celica
Shin19 F!Hector
Fancy Bard by MeatOfJustice
t2 Ephriam Map Anims by Sme, milom
t2 Osbert sprite by Der, Pikmin
t1 Ephraim Map anims by SSHX, Zoramine Fae, Tetraspark
Female Brigand anims by Skitty
Female Mercenary anims by Agro
Female Fighter anims by Alusq
Female Warrior anims by FEGirls
Knight (U) Axe by Agro
Dragon (U) Myrrh Generic by Kemui52
Wyvern Rider Axe by flasuban
Brigand (F) by Skitty
Berserker (F) by Unknown
Valkyrie (M) Non-Religious by Der
Marksman by ArcherBias
Swordmaster Reskin F by Russel Clark
L95 - Void Dragon
Airship by 2daemoth, ZarG, 7743

Class Cards:
Knight (U) Axe by Der, Epicer

Music:
dragstondev (FE5 cover)
Christopher Larkin
Motoi Sakuraba
Orange Free Sounds
Hans Zimmer
Hitoshi Sakimoto and Masaharu Iwata
Ibizu (Circle Sanbondo)

Backgrounds:
Eldritch Abomination
Majin Tensei